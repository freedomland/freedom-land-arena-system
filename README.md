# Freedom Land Arena System

Proof of concept of arena. Still require some work, but already working like a charm.

### Requirements:

- Our fork of CoreScripts
- Better Lua Random
- mpWidgets
- Timer Api Ex

### How to install

1. Put HouseSystem.lua to CoreScripts/scripts
2. Put Messages directory to CoreScripts/data

3. Add require("FL_Arena") somewhere in top of ServerCore.lua and add:
FL_Arena.Init() in OnServerInit() before tes3mp.SetPluginEnforcementState(config.enforcePlugins)

4. You also need client-side plugin to use, can be found in blob directory.

### License

Freedom Land Arena System (c) 2019 Freedom Land Team, GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

