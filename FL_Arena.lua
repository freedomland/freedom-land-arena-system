tableHelper = require("tableHelper")
logicHandler = require("logicHandler")
timerApiEx = require("timerApiEx")
mpWidgets = require("mpWidgets")
CommonFunctions = require("CommonFunctions")
activateInterface = require("activateInterface")
itemInterface = require("itemInterface")

require("betterLuaRandom")
require("config")

local instruction = "#FFB143" .. "Добро пожаловать на арену!\n\n" ..
"Для того, чтобы поучаcтвовать необходимо зарегистрироваться.\n\n" ..
"Правила:\n" ..
"1. Соперник подбирается автоматически и рандомно. Соперник будет вашего уровня +-3.\n" ..
"2. После того, как вас телепортируют на арену, у вас будет 10 секунд чтобы подготовиться к бою.\n" ..
"3. На арене нельзя летать или телепортировать, и использовать любой вид вмешательства. Битва идёт до победного конца. Если игрок вылетел во время битвы или вышел с сервера -- очки не засчитываются.\n" ..
"4. На арене разбросаны обелиски, они дают как положительные так и отрицательные эффеты. Используйте их только в крайних случаях, потому что шанс получить отрицательный эффект и умереть тоже есть.\n" ..
"5. На арене возможно использовать только 3 зелья восстановления.\n" ..
"6. Cамодельные предметы запрещены.\n" ..
"7. Время битвы ограничено 5 минутами. После 1 минуты вы будете отравлены, на 5 минуте вы гарантированно умрёте. Затягивать бой не имеет смысла.\n" ..
"8. Учавствовать можно неограниченое кол-во раз."

local spells = {
	"fm_s_arena_regen3hp60",
	"fm_s_arena_regen200hp1",
	"fm_s_arena_shield20frost30",
	"fm_s_arena_shield20fire30",
	"fm_s_arena_hurt20hp1",
	"fm_s_arena_poison20comm1",
	"fm_s_arena_blindness75comm5",
	"fm_s_arena_fortify10attack60",
	"fm_s_arena_fortify30hp60",
	"fm_s_arena_fortify25speed60",
	"fm_s_arena_hurt20lighting1",
	"fm_s_arena_hurt20fire1",
	"fm_s_arena_hurt20frost1",
	"fm_s_arena_shield20comm30",
	"fm_s_arena_shield20lightning30"
}

local coords = {
	["Arena"] = {
		[1] = {
			X1 = 4269.729492,
			Y1 = 2911.514648,
			X2 = 4306.983887,
			Y2 = 3407.862549
		},
	
		[2] = {
			X1 = 4348.792969,
			Y1 = 5498.347656,
			X2 = 4331.187988,
			Y2 = 4880.348633
		},
		
		Z = 11879.332031,
		
		zZ = 11865.644531,
		
		respSpot = 2
	}
}

local coords_taken = {
	["Arena"] = {}
}

local service_data = {
	["Arena"] = {}
}

local seccount = 10

Methods = {}

-- Constructor
function Methods.Init()
	activateInterface.AddRefid("flaenia amiulusus", OnFlaeniaActivate)
	activateInterface.AddRefid("fm_a_arena_switch01", OnArenaSwitchActivate)
	
	-- todo: add all Arenas
	itemInterface.AddCell("Arena", "$custom", OnCustomItemUsed)
	itemInterface.AddCell("Arena", "p_restore", OnPotionUsed)
	itemInterface.AddCell("Arena", "p_fortify", OnPotionUsed)
end

-- Signals
function OnFlaeniaActivate(pid, cellDescription, objectRefId, refNum, mpNum)
	local menuId = config.customMenuIds.FLArenaReg
	mpWidgets.New(menuId, instruction)
	mpWidgets.AddButton(menuId, 0, "Зарегистрироваться", function() Registrate(pid, "Arena") end)
	mpWidgets.AddButton(menuId, 1, "Поговорить", function() CommonFunctions.ForceGreet(pid, "flaenia amiulusus") end)
	mpWidgets.AddButton(menuId, 2, "Отмена", nil)
	mpWidgets.Show(pid, menuId, 0)
end

function OnArenaSwitchActivate(pid, cellDescription, objectRefId, refNum, mpNum)
	if service_data[cell][pid].obelisk + 15 - os.time() < 0 then
		CommonFunctions.Cast(pid, getRandValue(spells), "player", "player")
		service_data[cell][pid].obelisk = os.time()
		return true
	end
end

function OnCustomItemUsed(pid, cellDescription, objectRefId)
	tes3mp.MessageBox(pid, -1, "#FF0000" .. "Здесь нельзя использовать кастомные предметы.")
end

function OnPotionUsed(pid, objectRefId)
	local items = service_data[arena][pid].itemused
		
	if service_data[arena][pid].itemused > 3 then
		tes3mp.MessageBox(pid, -1, "#FF0000" .. "Вы исчерпали лимит зельий.")
		return
	end
		
	items = items + 1
	service_data[arena][pid].itemused = items
	
	tes3mp.SendItemUse(pid)
end

-- Utils
function LastPlayers(arena)
	local p = 0
	
	for key, value in pairs(coords_taken[arena]) do
		if value ~= -1 then
			p = p + 1
		end
	end
	
	return p
end

function Cleanup()

	for pid, value in pairs(coords_taken["Arena"]) do
		for spell = 1,#spells do
			CommonFunctions.RemoveSpellEffects(pid, spells[spell])
		end
		
		CommonFunctions.RemoveSpellEffects(pid, "fm_s_arena_timeout_poison1_1")
		CommonFunctions.RemoveSpellEffects(pid, "fm_s_arena_timeout_poison1_3")
		CommonFunctions.RemoveSpellEffects(pid, "fm_s_arena_timeout_poison1_5")
		CommonFunctions.RemoveSpellEffects(pid, "fm_s_arena_suddendeath")
	
		Players[pid]:RestoreHealth()
		Players[pid]:RestoreMagicka()
		Players[pid]:RestoreFatigue()
		tes3mp.SendStatsDynamic(pid)
	end

	coords_taken = {
		["Arena"] = {}
	}
	
	service_data = {
		["Arena"] = {}
	}
end

-- Main
function Registrate(pid, arena)
	if tableHelper.getCount(Players) == 1 then
		tes3mp.MessageBox(pid, -1, "#FFB143" .. "Для того, чтобы учавствовать на арене, нужно как минимум 2 игрока на сервере.")
		return
	end

	if service_data[arena].taken == true then
		tes3mp.MessageBox(pid, -1, "#FFB143" .. "Арена сейчас занята, попробуйте позже.")
		return
	end
	
	if coords_taken[arena][pid] ~= nil then
		tes3mp.MessageBox(pid, -1, "#FFB143" .. "Вы уже зарегистрировались.")
		return
	end
	
	if service_data[arena].iniciator == nil then
		service_data[arena].iniciator = pid
	end

	for i = 1,tableHelper.getCount(coords[arena]) do
		if tableHelper.containsValue(coords_taken[arena], i, false) == false then
			coords_taken[arena][pid] = i
			service_data[arena][pid] = {}
			service_data[arena][pid].itemused = 0
			service_data[arena][pid].obelisk = 0
			break
		end
	end
	
	if service_data[arena].msg == nil then
		SearchForPlayers(pid, arena)
	end
	
	if LastPlayers(arena) == coords[arena].respSpot then
		service_data[arena].taken = true
		TpAllToArena(arena)
	end
end

function SearchForPlayers(pid, arena)	
	local iniciator = Players[service_data[arena].iniciator]
	for index, player in pairs(Players) do -- search for players
		if iniciator.data.stats.level == player.data.stats.level + 3 
			or iniciator.data.stats.level == player.data.stats.level
			or player.data.stats.level == iniciator.data.stats.level - 3 then
			Players[player.pid]:Message("// #FFB143" .. "Игрок " .. iniciator.chat.accountName .. " #FFB14" .. "приглашает сразиться на арене 1х1. Введите /arena чтобы поучаcтвовать.\n")
		end
	end
	
	service_data[arena].msg = true
end

function Fight(arena)
	local countdown = timerApiEx.GetCurrentLoopNum("Fight")
	
	if countdown > 0 then
		for pid, value in pairs(coords_taken[arena]) do
			tes3mp.MessageBox(pid, -1, "Бой начнётся через " .. countdown .. " секунд")
		end
		
		return
	end

	for pid, value in pairs(coords_taken[arena]) do
		local spot = coords_taken[arena][pid]
		tes3mp.SetPos(pid, coords[arena][spot].X2, coords[arena][spot].Y2, coords[arena].Z)
		tes3mp.SendPos(pid)
		
		tes3mp.PlaySpeech(pid, "Fx\\envrn\\bell4.wav")
		tes3mp.MessageBox(pid, -1, "Бой начался!")
	end
	
	timerApiEx.CreateTimer("SuddenDeath_01", time.seconds(60), function() PoisonPlayers(1) end)
	timerApiEx.CreateTimer("SuddenDeath_02", time.seconds(60*2), function() PoisonPlayers(3) end)
	timerApiEx.CreateTimer("SuddenDeath_03", time.seconds(60*3), function() PoisonPlayers(5) end)
	timerApiEx.CreateTimer("SuddenDeath_04", time.seconds(60*4), function() PoisonPlayers(-1) end)
	timerApiEx.StartTimer("SuddenDeath_01")
	timerApiEx.StartTimer("SuddenDeath_02")
	timerApiEx.StartTimer("SuddenDeath_03")
	timerApiEx.StartTimer("SuddenDeath_04")
end

function TpAllToArena(arena)
	for pid, value in pairs(coords_taken[arena]) do
		local location = {
			cell = tes3mp.GetCell(pid),
			posX = tes3mp.GetPosX(pid),
			posY = tes3mp.GetPosY(pid),
			posZ = tes3mp.GetPosZ(pid),
			rotX = tes3mp.GetRotX(pid),
			rotZ = tes3mp.GetRotZ(pid)
		}
		
		Players[pid].data.customVariables.location = location
		
		tes3mp.SendMessage(pid, "// #FFB143" .. "Вы были телепортированы на арену.\n", false)
		
		local spot = coords_taken[arena][pid]
		local pidPos = coords[arena][spot]
		CommonFunctions.COC(pid, arena, {posX = pidPos.X1, posY = pidPos.Y1, posZ = pidPos.Z})
	end
	
	timerApiEx.CreateLoopTimer("Fight", time.seconds(1), seccount, function() Fight("Arena") end)
	timerApiEx.StartTimer("Fight")
end

function Methods.OnPlayerDeath(pid, arena)
	local spot = coords_taken[arena][pid]
	if spot ~= nil then
		tes3mp.SetPos(pid, coords[arena][spot].X1, coords[arena][spot].Y1, coords[arena].zZ)
		tes3mp.SendPos(pid)
		coords_taken[arena][pid] = -1
		CommonFunctions.ResurrectPlayer(pid)
		CommonFunctions.PlayerControlls(pid, false)
	
		CheckForResults(arena)
	end
end

function CheckForResults(arena)
	if LastPlayers(arena) == 1 then -- we have winer
		ShowResultsWiner(arena)
	elseif LastPlayers(arena) == 0 then -- draw
		ShowResultsDraw(arena)
	end
end

function PoisonPlayers(stage)
	local arena = "Arena"

	if stage == -1 then
		for pid, value in pairs(coords_taken[arena]) do
			CommonFunctions.Cast(pid, "fm_s_arena_suddendeath", "player", "player")
		end
	else
		for pid, value in pairs(coords_taken[arena]) do
			CommonFunctions.Cast(pid, "fm_s_arena_timeout_poison1_" .. stage, "player", "player")
		end
	end
end

-- GUI
function ShowResultsWiner(arena)
	local msg = "Результаты:\n\n"

	for pid, value in pairs(coords_taken[arena]) do
		if value == -1 then
			msg = msg .. Players[pid].chat.accountName .. " -- #C14317" .. "выбыл\n\n"
			logicHandler.RunConsoleCommandOnPlayer(pid, "enableplayercontrols", false)
		else
			msg = msg .. Players[pid].chat.accountName .. " -- #30C117" .. "победитель!\n\n"
		end
	end
	
	for pid, value in pairs(coords_taken[arena]) do
		local location = Players[pid].data.customVariables.location
		mpWidgets.MessageBox(pid, msg, "NORMAL")
		CommonFunctions.COC(pid, location.cell, {posX = location.posX, posY = location.posY, posZ = location.posZ}, {rotX = location.rotX, rotZ = location.rotZ})
	end
	
	Cleanup()
end

function ShowResultsDraw(arena)
	for pid, value in pairs(coords_taken[arena]) do
		mpWidgets.MessageBox(pid, "#C14317" .. "Ничья!\n\nВ следующий раз повезёт.", "NORMAL")
	end
	
	Cleanup()
end

return Methods
